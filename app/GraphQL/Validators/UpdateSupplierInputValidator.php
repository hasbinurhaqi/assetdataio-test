<?php

namespace App\GraphQL\Validators;

use Nuwave\Lighthouse\Validation\Validator;

class UpdateSupplierInputValidator extends Validator
{
    /**
     * Return the validation rules.
     *
     * @return array<string, array<mixed>>
     */
    
    public function rules(): array
    {
        return [
            'name' => [ 'required' ],
            'public_id' => [ 'required' ],
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'The name field is not available',
            'public_id.required' => 'The public_id field is not available',
        ];
    }
}
