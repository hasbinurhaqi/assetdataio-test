<?php

namespace App\GraphQL\Validators;

use Nuwave\Lighthouse\Validation\Validator;

class DeleteSupplierInputValidator extends Validator
{
    /**
     * Return the validation rules.
     *
     * @return array<string, array<mixed>>
     */
    public function rules(): array
    {
        return [
            'public_id' => [ 'required' ],
        ];
    }

    public function messages(): array
    {
        return [
            'public_id.required' => 'The public_id field is not available',
        ];
    }
}
