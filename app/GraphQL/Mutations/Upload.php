<?php

namespace App\GraphQL\Mutations;

use Auth;
use App\Models\User;

class Upload
{
    /**
     * Upload a file, store it on the server and return the path.
     *
     * @param  mixed  $root
     * @param  array<string, mixed>  $args
     * @return string|null
     */

    public function rules(): array
    {
        return [
            'file' => ['required', 'mimes:png,jpg,jpeg', 'max:300'],
        ];
    }

    public function __invoke($root, array $args): ?string
    {
        /** @var \Illuminate\Http\UploadedFile $file */
        $file = $args['file'];
        $ext = strtolower($file->getClientOriginalExtension());
        $sze = strtolower($file->getSize());

        if ($ext != 'png' && $ext != 'jpeg' && $ext != 'jpg') {
            return 'invalid file extention!';
        }

        if ($sze > 300000) {
            return 'invalid file size!';
        }

        $uploaded = $file->storePublicly('uploads');

        User::where('public_id', Auth::user()->public_id)->update([
            'photo' => $uploaded
        ]);

        return $uploaded;
    }
}
