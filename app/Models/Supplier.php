<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Supplier extends Model
{
    protected $table = 'supplier';

    protected $fillable = [
        'public_id',
        'name',
        'country',
        'state',
        'city',
        'street',
        'zip',
        'cp',
        'pcp',
        'acp',
    ];

    protected $timetamps = true;

    protected $primaryKey = 'public_id';
    
    protected $keyType = 'string';

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($post) {
            $post->public_id = (string) Str::uuid();
        });
    }
}
