<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    protected $table = 'users_meta';

    protected $fillable = [
        'key',
        'value',
        'user_id',
    ];

    protected $timetamps = true;
}
