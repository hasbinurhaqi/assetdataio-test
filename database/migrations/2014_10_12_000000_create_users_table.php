<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('public_id', 100)->unique();
            $table->string('firstname', 15);
            $table->string('lastname', 15)->nullable();
            $table->string('email')->unique();
            $table->string('phonenumber', 12)->unique();
            $table->string('password');
            $table->longText('address')->nullable();
            $table->longText('photo')->nullable();
            $table->timestamp('login_at')->nullable();
            $table->longText('login_loc')->nullable();
            $table->longText('user_agent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
