<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Str;

class UsersSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {   
        DB::table('users')->insert([
            'public_id' => (string)Str::uuid(),
            'firstname' => 'Hasbi',
            'lastname' => 'Nurhaqi',
            'email' => 'hasbinurhaqy@gmail.com',
            'phonenumber' => '081316628806',
            'password' => Hash::make('admin123!'),
            'address' => 'Perumahan Griya Soka Blok U No.30, Kab.Bogor',
            'photo' => NULL,
            'login_at' => NULL,
            'login_loc' => NULL,
            'user_agent' => NULL,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
    }
}