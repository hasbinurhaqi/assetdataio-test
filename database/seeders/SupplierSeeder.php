<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Str;

class SupplierSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('supplier')->insert([
            'public_id' => (string)Str::uuid(),
            'name' =>  'AssetData.IO',
            'country' => 'ID',
            'state' => 'DKI JAKARTA',
            'city' => 'JAKARTA',
            'street' => 'Jln.Gatot Subroto No. 1',
            'zip' => '1234',
            'cp' => 'Hasbi Nurhaqi (081316628806)',
            'pcp' => 'Lokapita (081316628804)',
            'acp' => 'Tuki (081316628805)',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
    }
}