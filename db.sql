CREATE TABLE "public"."migrations" ( 
  "id" INTEGER NOT NULL DEFAULT nextval('migrations_id_seq'::regclass) ,
  "migration" VARCHAR(255) NOT NULL,
  "batch" INTEGER NOT NULL,
  CONSTRAINT "migrations_pkey" PRIMARY KEY ("id")
);
CREATE TABLE "public"."oauth_access_tokens" ( 
  "id" VARCHAR(100) NOT NULL,
  "user_id" UUID NULL,
  "client_id" UUID NOT NULL,
  "name" VARCHAR(255) NULL,
  "scopes" TEXT NULL,
  "revoked" BOOLEAN NOT NULL,
  "created_at" TIMESTAMP NULL,
  "updated_at" TIMESTAMP NULL,
  "expires_at" TIMESTAMP NULL,
  CONSTRAINT "oauth_access_tokens_pkey" PRIMARY KEY ("id")
);
CREATE TABLE "public"."oauth_auth_codes" ( 
  "id" VARCHAR(100) NOT NULL,
  "user_id" BIGINT NOT NULL,
  "client_id" UUID NOT NULL,
  "scopes" TEXT NULL,
  "revoked" BOOLEAN NOT NULL,
  "expires_at" TIMESTAMP NULL,
  CONSTRAINT "oauth_auth_codes_pkey" PRIMARY KEY ("id")
);
CREATE TABLE "public"."oauth_clients" ( 
  "id" UUID NOT NULL,
  "user_id" UUID NULL,
  "name" VARCHAR(255) NOT NULL,
  "secret" VARCHAR(100) NULL,
  "provider" VARCHAR(255) NULL,
  "redirect" TEXT NOT NULL,
  "personal_access_client" BOOLEAN NOT NULL,
  "password_client" BOOLEAN NOT NULL,
  "revoked" BOOLEAN NOT NULL,
  "created_at" TIMESTAMP NULL,
  "updated_at" TIMESTAMP NULL,
  CONSTRAINT "oauth_clients_pkey" PRIMARY KEY ("id")
);
CREATE TABLE "public"."oauth_personal_access_clients" ( 
  "id" BIGINT NOT NULL DEFAULT nextval('oauth_personal_access_clients_id_seq'::regclass) ,
  "client_id" UUID NOT NULL,
  "created_at" TIMESTAMP NULL,
  "updated_at" TIMESTAMP NULL,
  CONSTRAINT "oauth_personal_access_clients_pkey" PRIMARY KEY ("id")
);
CREATE TABLE "public"."oauth_refresh_tokens" ( 
  "id" VARCHAR(100) NOT NULL,
  "access_token_id" VARCHAR(100) NOT NULL,
  "revoked" BOOLEAN NOT NULL,
  "expires_at" TIMESTAMP NULL,
  CONSTRAINT "oauth_refresh_tokens_pkey" PRIMARY KEY ("id")
);
CREATE TABLE "public"."personal_access_tokens" ( 
  "id" BIGINT NOT NULL DEFAULT nextval('personal_access_tokens_id_seq'::regclass) ,
  "tokenable_type" VARCHAR(255) NOT NULL,
  "tokenable_id" BIGINT NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "token" VARCHAR(64) NOT NULL,
  "abilities" TEXT NULL,
  "last_used_at" TIMESTAMP NULL,
  "created_at" TIMESTAMP NULL,
  "updated_at" TIMESTAMP NULL,
  CONSTRAINT "personal_access_tokens_pkey" PRIMARY KEY ("id")
);
CREATE TABLE "public"."social_providers" ( 
  "id" BIGINT NOT NULL DEFAULT nextval('social_providers_id_seq'::regclass) ,
  "user_id" UUID NULL,
  "provider" VARCHAR(255) NOT NULL,
  "provider_id" VARCHAR(255) NOT NULL,
  "created_at" TIMESTAMP NULL,
  "updated_at" TIMESTAMP NULL,
  CONSTRAINT "social_providers_pkey" PRIMARY KEY ("id")
);
CREATE TABLE "public"."supplier" ( 
  "id" BIGINT NOT NULL DEFAULT nextval('supplier_id_seq'::regclass) ,
  "public_id" VARCHAR(100) NOT NULL,
  "name" VARCHAR(100) NOT NULL,
  "country" VARCHAR(255) NULL,
  "state" VARCHAR(255) NULL,
  "city" VARCHAR(255) NULL,
  "street" VARCHAR(255) NULL,
  "zip" VARCHAR(10) NULL,
  "cp" VARCHAR(255) NULL,
  "pcp" VARCHAR(255) NULL,
  "acp" VARCHAR(255) NULL,
  "created_at" TIMESTAMP NULL,
  "updated_at" TIMESTAMP NULL,
  CONSTRAINT "supplier_pkey" PRIMARY KEY ("id")
);
CREATE TABLE "public"."users" ( 
  "id" BIGINT NOT NULL DEFAULT nextval('users_id_seq'::regclass) ,
  "public_id" VARCHAR(100) NOT NULL,
  "firstname" VARCHAR(15) NOT NULL,
  "lastname" VARCHAR(15) NULL,
  "email" VARCHAR(255) NOT NULL,
  "phonenumber" VARCHAR(12) NOT NULL,
  "password" VARCHAR(255) NOT NULL,
  "address" TEXT NULL,
  "photo" TEXT NULL,
  "login_at" TIMESTAMP NULL,
  "login_loc" TEXT NULL,
  "user_agent" TEXT NULL,
  "created_at" TIMESTAMP NULL,
  "updated_at" TIMESTAMP NULL,
  "avatar" VARCHAR(255) NULL,
  CONSTRAINT "users_pkey" PRIMARY KEY ("id")
);
CREATE TABLE "public"."users_meta" ( 
  "id" BIGINT NOT NULL DEFAULT nextval('users_meta_id_seq'::regclass) ,
  "key" TEXT NOT NULL,
  "value" TEXT NULL,
  "user_id" INTEGER NULL,
  "created_at" TIMESTAMP NULL,
  "updated_at" TIMESTAMP NULL,
  CONSTRAINT "users_meta_pkey" PRIMARY KEY ("id")
);
ALTER TABLE "public"."migrations" DISABLE TRIGGER ALL;
ALTER TABLE "public"."oauth_access_tokens" DISABLE TRIGGER ALL;
ALTER TABLE "public"."oauth_auth_codes" DISABLE TRIGGER ALL;
ALTER TABLE "public"."oauth_clients" DISABLE TRIGGER ALL;
ALTER TABLE "public"."oauth_personal_access_clients" DISABLE TRIGGER ALL;
ALTER TABLE "public"."oauth_refresh_tokens" DISABLE TRIGGER ALL;
ALTER TABLE "public"."personal_access_tokens" DISABLE TRIGGER ALL;
ALTER TABLE "public"."social_providers" DISABLE TRIGGER ALL;
ALTER TABLE "public"."supplier" DISABLE TRIGGER ALL;
ALTER TABLE "public"."users" DISABLE TRIGGER ALL;
ALTER TABLE "public"."users_meta" DISABLE TRIGGER ALL;
INSERT INTO "public"."migrations" ("id", "migration", "batch") VALUES (42, '2014_10_12_000000_create_users_table', 1);
INSERT INTO "public"."migrations" ("id", "migration", "batch") VALUES (43, '2016_06_01_000001_create_oauth_auth_codes_table', 1);
INSERT INTO "public"."migrations" ("id", "migration", "batch") VALUES (44, '2016_06_01_000002_create_oauth_access_tokens_table', 1);
INSERT INTO "public"."migrations" ("id", "migration", "batch") VALUES (45, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1);
INSERT INTO "public"."migrations" ("id", "migration", "batch") VALUES (46, '2016_06_01_000004_create_oauth_clients_table', 1);
INSERT INTO "public"."migrations" ("id", "migration", "batch") VALUES (47, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1);
INSERT INTO "public"."migrations" ("id", "migration", "batch") VALUES (48, '2019_11_19_000000_update_social_provider_users_table', 1);
INSERT INTO "public"."migrations" ("id", "migration", "batch") VALUES (49, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO "public"."migrations" ("id", "migration", "batch") VALUES (50, '2022_03_19_032807_create_users_meta_table', 1);
INSERT INTO "public"."migrations" ("id", "migration", "batch") VALUES (51, '2022_03_19_033809_create_supplier_table', 1);
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('b6a0e8d296fea6cbd52c43a6b80234120ec1caa133b82e657a8cf34cdf8b78851eb8e4b250814b70', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T08:57:21.000Z', '2022-03-20T08:57:21.000Z', '2023-03-20T08:57:21.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('8d0fe24d3cc9191039da23b64898e8a9cf735adec0171a573355c9570e371a5a8f42fb6c0c5ccd67', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T09:00:41.000Z', '2022-03-20T09:00:41.000Z', '2023-03-20T09:00:41.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('d4fc743f339eb5fa4fc0b6063954d634f47ef648d8fb82f60884812944edaf74bbe80dafaa13054d', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T09:02:29.000Z', '2022-03-20T09:02:29.000Z', '2023-03-20T09:02:29.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('4671a132082fb60245ae341dde754e630acff4db415414f57f2b4041c35b7cab6bcd099621a3eaa2', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T09:19:40.000Z', '2022-03-20T09:19:40.000Z', '2023-03-20T09:19:40.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('0ac5baacdd2feba494fd4eeae318e262779617197689b920d3f851da01eeb9ccbbf9dfc6291589d0', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T09:20:12.000Z', '2022-03-20T09:20:12.000Z', '2023-03-20T09:20:12.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('ebfdef5d95cad4c0029163150d81303d8450e1248b53ffa1294167e19182498e9ce49e33023f15b0', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T09:35:31.000Z', '2022-03-20T09:35:31.000Z', '2023-03-20T09:35:31.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('8b517ab3a0c87e503c4883a04ae4fa718c66e2bf15f256c00c93c54fa307642465fb72e72dfed075', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T09:35:59.000Z', '2022-03-20T09:35:59.000Z', '2023-03-20T09:35:59.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('432e2a19435b57687110a79421be7a39ab6e80f258da09f1c5e8c8f6f094d15b9d051e6fd8b09c05', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T09:36:57.000Z', '2022-03-20T09:36:57.000Z', '2023-03-20T09:36:57.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('213014d9f5b6e61646ced5474990d5af41677f115ec64c0f9fc73d8d4784e690c715df6947676938', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T09:37:29.000Z', '2022-03-20T09:37:29.000Z', '2023-03-20T09:37:29.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('e9650860190537d42b2d094849767ebbfbb7906e4a9da2f63845cd65d19025f55441dcff095bc779', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T09:38:37.000Z', '2022-03-20T09:38:37.000Z', '2023-03-20T09:38:37.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('8ed8c3a6e1bca9708bff0a039e5eedd6c38c7ab20fdde6442dda51cdfe51b1debae4ef31b7399e27', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T09:43:58.000Z', '2022-03-20T09:43:58.000Z', '2023-03-20T09:43:58.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('8f2179da9076cb2a5e2c18513568a8ae3eeb4b0557062936657d5a71331f2a7bb8a2dba6495f9e23', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T09:54:01.000Z', '2022-03-20T09:54:01.000Z', '2023-03-20T09:54:01.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('694e9d949c2b8ef962a6b38514f3e0505ec6f0f4fcbda0b7daf0c1e8616c273f16cc8298c8fb7940', 'a76cb7fa-d79a-410e-9834-06d474c4fbd9', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T10:06:30.000Z', '2022-03-20T10:06:30.000Z', '2023-03-20T10:06:30.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('197ef529f4b8ae6a66086d83a3c192f3cfa04c8195d38f72be41317e1905ba5b8338d40861b38633', '77aa205b-f2f3-4bec-817b-7ae317d34d5b', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T10:08:18.000Z', '2022-03-20T10:08:18.000Z', '2023-03-20T10:08:18.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('47e04994d44f155857264d5c8c5f2a312cc76d3b1f32d0aeb278a96702071cad526b145952cc5025', '96aedfbe-1987-4e69-913b-964a03ed4157', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T10:09:29.000Z', '2022-03-20T10:09:29.000Z', '2023-03-20T10:09:29.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('6e24e9878f80df096880650c23048df401c38d69e75fce2c90003662e74d947a9fc397f86ac0384a', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T10:16:18.000Z', '2022-03-20T10:16:18.000Z', '2023-03-20T10:16:18.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('4d5babad48ab5b0ae48f2062fc09864d9ec8d56974e16cf5effd38e9c6fd85439198457402995e45', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:34:28.000Z', '2022-03-20T11:34:28.000Z', '2023-03-20T11:34:28.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('a739ef2eb2bcac87af2ade2b4886ef29a611740ab21c208809e650d1ab6aa5614ccf28372457ea13', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:34:59.000Z', '2022-03-20T11:34:59.000Z', '2023-03-20T11:34:59.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('5763173bc9a8ada00b7c93b07a476b5546eeb4b0019525cd9ca7d0f2982769fc0c20f487c61cc908', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:35:52.000Z', '2022-03-20T11:35:52.000Z', '2023-03-20T11:35:52.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('c8bb65bed7844efdd5ae702d03828dc17890978c55ab7da8d7276ab3382bdeec0b7f424ca109488a', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:36:06.000Z', '2022-03-20T11:36:06.000Z', '2023-03-20T11:36:06.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('e20f3519af63b93370318c9b71fc3ac5cd1d21e934bd19ba87111a5eb731db30e479c3e25713b68e', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:36:55.000Z', '2022-03-20T11:36:55.000Z', '2023-03-20T11:36:55.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('f2efe2f10aee08c6d07a9612e7ba599ef73c28780b868801873b27072576493962b38bc2d90a579c', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:38:06.000Z', '2022-03-20T11:38:06.000Z', '2023-03-20T11:38:06.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('864d1d7c8046163392a0b710b8f39d238d48d106d44b883f066935a0c63f8af1704fd93172e9ec20', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:39:18.000Z', '2022-03-20T11:39:18.000Z', '2023-03-20T11:39:18.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('79d6ac70976cad7f664aebf0b713478cbbf5248f044e3c1ca43ca8f7afe353b7a2ebe97cda45ea06', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:41:52.000Z', '2022-03-20T11:41:52.000Z', '2023-03-20T11:41:52.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('79e7a84360635ab841f74255072bb8cd3ff806d3b3cb39e94ece984789b4074e713026845ae62892', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:42:07.000Z', '2022-03-20T11:42:07.000Z', '2023-03-20T11:42:07.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('c7b4808e29bcc05412fc6675cf65870abd959e9e2825da2f9aab84868814612196de97f95a50c89c', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:42:19.000Z', '2022-03-20T11:42:19.000Z', '2023-03-20T11:42:19.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('30e1f380a8ad2f5b71def0f3410b41b8d0739a9f91cb30f0fbee9f1328336db8b516715080de9c24', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:42:29.000Z', '2022-03-20T11:42:29.000Z', '2023-03-20T11:42:29.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('c63f7507767f6e9e68abe477a0cc3bdabfbf287984b269fdd6dc4e2b2939ff73ae430a583a075cc1', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:42:34.000Z', '2022-03-20T11:42:34.000Z', '2023-03-20T11:42:34.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('3ed814242bd079d83aee33a6b93607d258ca91ed5312576e99459153343193b7636a600ccb554df7', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:42:42.000Z', '2022-03-20T11:42:42.000Z', '2023-03-20T11:42:42.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('c4d51f05c3b8c4b2023174a7af942d8a2115e88b65f64f6ee8f7991c671ef7a8e6f713164bb48d6f', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:48:54.000Z', '2022-03-20T11:48:54.000Z', '2023-03-20T11:48:54.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('eca165793029b94a354e9ea6eb4d67f420aadb93cdaac42dd4151f336f75bb4edf0186f24690c592', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:49:41.000Z', '2022-03-20T11:49:41.000Z', '2023-03-20T11:49:41.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('82905a6d0b2aca6ba358bbe43d368f3d938d12d4cd9454f344199fa991086dce6ff1482433e2e8fa', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:50:12.000Z', '2022-03-20T11:50:12.000Z', '2023-03-20T11:50:12.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('c7fe9bbe19ffd46fb7f748fab5253ffff4ddbab866246aae06ffb31dc3c666ac79048b2f40036367', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:50:30.000Z', '2022-03-20T11:50:30.000Z', '2023-03-20T11:50:30.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('850ca47d50d5d0b22a1032ed93fdd1b3fc1f457dd3a01ca1b96f5a21309112254161d03a5ce84f1d', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:50:36.000Z', '2022-03-20T11:50:36.000Z', '2023-03-20T11:50:36.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('f26f128713b29991db2c2002da6a0d35ada4ea280eb8e93b5f32c782db5adc5a86cc34df89727c86', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:51:46.000Z', '2022-03-20T11:51:46.000Z', '2023-03-20T11:51:46.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('9a6dccb9065df89f6eb5d506d25d6146a54f87b2cd2a96a2b864cdf4b80586d3b0aa55eb41ba5553', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:51:50.000Z', '2022-03-20T11:51:50.000Z', '2023-03-20T11:51:50.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('14b21a62a612bf874ef46822ea004952ab7dee781ef20c06d7248523f5e635e66d0f4ba0559890d6', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:53:15.000Z', '2022-03-20T11:53:15.000Z', '2023-03-20T11:53:15.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('2eb48573f3069b36d4f89314ea51c0263bc68720adb3bbaf0d2b395892fc2bd37f075479ab445bbe', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:53:26.000Z', '2022-03-20T11:53:26.000Z', '2023-03-20T11:53:26.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('20f5ddc8f5c061503f33da8220912e806872939ac39e1ea77ef88905a81517fee04d24b8bbbecb4b', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:53:32.000Z', '2022-03-20T11:53:32.000Z', '2023-03-20T11:53:32.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('15f506ea9f00c953675b3c0d9df3fff485e35ed5509e7f908b23ba27f87db50da0691d0ba8921cee', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:54:16.000Z', '2022-03-20T11:54:16.000Z', '2023-03-20T11:54:16.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('09808f7f31468c6ecdec3bf11c56bd1cb77dfff7eb1d0ceba6e34db5af1d0c33b9311f65a30c9722', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:54:53.000Z', '2022-03-20T11:54:53.000Z', '2023-03-20T11:54:53.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('01d0d5fe8dbdea115d0fc87a83bcc0d98d0e03518370f21d49bc17e1eb0a8ac2a5c07bfb4eba22d3', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:55:09.000Z', '2022-03-20T11:55:09.000Z', '2023-03-20T11:55:09.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('dea14de8aa9178af4abf8b5ba6b9fe45cb351ea35211e38fdcdfddf89b9bae1a84193640555f5426', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:56:47.000Z', '2022-03-20T11:56:47.000Z', '2023-03-20T11:56:47.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('5459a5fcb018b05767bb5b920d6d68db39e23741fe5d5479939c316e0f373dbf2898a948e5614e8f', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:57:31.000Z', '2022-03-20T11:57:31.000Z', '2023-03-20T11:57:31.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('47b042975d90f85f1ddeadf75581b550bc35473f35cc5038f4f0a383e64d5ebaa999234cc1fadd79', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:59:00.000Z', '2022-03-20T11:59:00.000Z', '2023-03-20T11:59:00.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('3d03320a98c65965583b5abaabe8df5b17e12aed923cbc4d8b4a2b52f8c573fe9cfa88f746c50bd0', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:59:27.000Z', '2022-03-20T11:59:27.000Z', '2023-03-20T11:59:27.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('13f6759821a1ff28fe39c5490360a934e19c3af34e2b2e12390ab34132fb1f36bbc3cf04f8bb50b3', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T11:59:51.000Z', '2022-03-20T11:59:51.000Z', '2023-03-20T11:59:51.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('d06c96859388c8d8cef353446924c536df2467582511ecce63feb22fbddfec90cd560ddebdc6d784', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T12:01:26.000Z', '2022-03-20T12:01:26.000Z', '2023-03-20T12:01:26.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('33dd0a206c538276c3ae74a6d1df796cb1700ad57420d4ac4e42554f4216b0e75a8f9571dbd91444', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T12:01:39.000Z', '2022-03-20T12:01:39.000Z', '2023-03-20T12:01:39.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('7b5f8614b6a990fea6a75cc75e4bd1e6dcc3fecfc9ff6ec7c7b44c6608ebb244639e80a695f0d812', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T12:02:39.000Z', '2022-03-20T12:02:39.000Z', '2023-03-20T12:02:39.000Z');
INSERT INTO "public"."oauth_access_tokens" ("id", "user_id", "client_id", "name", "scopes", "revoked", "created_at", "updated_at", "expires_at") VALUES ('1be253162ea7403ee60d130231619695e4f2fd80ae63e77f0e4d9161743a31dc8896ce3aa699aa6f', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', '95dda419-b2d5-4999-9925-452c5a393c0f', NULL, '[]', false, '2022-03-20T12:03:13.000Z', '2022-03-20T12:03:13.000Z', '2023-03-20T12:03:13.000Z');
INSERT INTO "public"."oauth_clients" ("id", "user_id", "name", "secret", "provider", "redirect", "personal_access_client", "password_client", "revoked", "created_at", "updated_at") VALUES ('95dda419-9b14-49c6-b08c-4b76dbdebb74', NULL, 'ASSETDATAIO Personal Access Client', 'oH9uK2vuQr4ja9wp58KvUB5Va0GqYY3CRuGMvgTE', NULL, 'http://localhost', true, false, false, '2022-03-20T08:55:34.000Z', '2022-03-20T08:55:34.000Z');
INSERT INTO "public"."oauth_clients" ("id", "user_id", "name", "secret", "provider", "redirect", "personal_access_client", "password_client", "revoked", "created_at", "updated_at") VALUES ('95dda419-b2d5-4999-9925-452c5a393c0f', NULL, 'ASSETDATAIO Password Grant Client', 'zF0ZUP79UeUYR8WmkFwe5jwXqGffrxsxiiASSjvt', 'users', 'http://localhost', false, true, false, '2022-03-20T08:55:34.000Z', '2022-03-20T08:55:34.000Z');
INSERT INTO "public"."oauth_personal_access_clients" ("id", "client_id", "created_at", "updated_at") VALUES ('1', '95dda419-9b14-49c6-b08c-4b76dbdebb74', '2022-03-20T08:55:34.000Z', '2022-03-20T08:55:34.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('4388495742ea05dafbb84f0b56ee670e82fe8aa9c75717f8a7c7f8c7a900238a4541fe2b660f4708', 'b6a0e8d296fea6cbd52c43a6b80234120ec1caa133b82e657a8cf34cdf8b78851eb8e4b250814b70', false, '2023-03-20T08:57:21.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('f36c80e367e4580dd94bae40bf3ccbbcb95967dc568a67e149f8bd8d25173fcdc0dd926441c99193', '8d0fe24d3cc9191039da23b64898e8a9cf735adec0171a573355c9570e371a5a8f42fb6c0c5ccd67', false, '2023-03-20T09:00:41.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('6d03a3dd994bdc1f28485547a1be7427c2f5dc8fc814cbb8d396a6ed08e54cee8d185a54191b2be6', 'd4fc743f339eb5fa4fc0b6063954d634f47ef648d8fb82f60884812944edaf74bbe80dafaa13054d', false, '2023-03-20T09:02:29.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('39654a51ba6a07c3cbf07bd061205b7f187cbfe6cf2f4cb8ab8ff7a5f1f7bd58e8ce6532f139d1c2', '4671a132082fb60245ae341dde754e630acff4db415414f57f2b4041c35b7cab6bcd099621a3eaa2', false, '2023-03-20T09:19:40.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('c7e27bcb87c4b215ffcc99b3a4a952dac636c3529fcbeac0930a1ea2ac42bd27a5328e2c0210b3be', '0ac5baacdd2feba494fd4eeae318e262779617197689b920d3f851da01eeb9ccbbf9dfc6291589d0', false, '2023-03-20T09:20:12.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('e97dc080e3fad20a34af5a7a43c992230a0db6e13eaad332a2273d2526e718e171bbacf86d2fcfcf', 'ebfdef5d95cad4c0029163150d81303d8450e1248b53ffa1294167e19182498e9ce49e33023f15b0', false, '2023-03-20T09:35:31.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('e609eef5746fff79c6644f27556cce133b4818e6b89622af0662e265f13805141e8dadd419d8d93f', '8b517ab3a0c87e503c4883a04ae4fa718c66e2bf15f256c00c93c54fa307642465fb72e72dfed075', false, '2023-03-20T09:35:59.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('91b09e487000d352dc5f5b9c011a53cf0f5f87c9d5479436abd77b34a2f45bbd34ceb4e0fa1ac5ad', '432e2a19435b57687110a79421be7a39ab6e80f258da09f1c5e8c8f6f094d15b9d051e6fd8b09c05', false, '2023-03-20T09:36:57.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('9b56925f8bd1274fab733b2141d0fb6296aac23b969a9d2d6bc0177387880f0ffd0caa567b61a1f5', '213014d9f5b6e61646ced5474990d5af41677f115ec64c0f9fc73d8d4784e690c715df6947676938', false, '2023-03-20T09:37:29.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('c487b2d7357ad20ae1bcd05ec69759c81a18cae9988b61ceee63e82f6981fb705952af847ec44e3e', 'e9650860190537d42b2d094849767ebbfbb7906e4a9da2f63845cd65d19025f55441dcff095bc779', false, '2023-03-20T09:38:37.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('9bf4cb191de98141fd155146a4c9fb2ac3406b10f90562187934a6cdd9d4f736136e80d639f60455', '8ed8c3a6e1bca9708bff0a039e5eedd6c38c7ab20fdde6442dda51cdfe51b1debae4ef31b7399e27', false, '2023-03-20T09:43:58.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('7d87da2873a733781d900c228ebd815a747f26fe4f0d1d95ae556bcb54bf12929c7057349a7e43a9', '8f2179da9076cb2a5e2c18513568a8ae3eeb4b0557062936657d5a71331f2a7bb8a2dba6495f9e23', false, '2023-03-20T09:54:01.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('4a5d4b0e372c872157bb616b3aa15ed01ce9b67d31bb29b0f083577bcd22222b3a475ec6a7de1010', '694e9d949c2b8ef962a6b38514f3e0505ec6f0f4fcbda0b7daf0c1e8616c273f16cc8298c8fb7940', false, '2023-03-20T10:06:30.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('d94d2a4ee1d67847831c27ed8d60d08dde38aa9f5f968a8a4f22822892c399adf831540020b8d238', '197ef529f4b8ae6a66086d83a3c192f3cfa04c8195d38f72be41317e1905ba5b8338d40861b38633', false, '2023-03-20T10:08:18.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('889fd2ce0b0d4ac74b39665c02e242f7831905a65d85c7a2654d1e522c5209f9b78c203cea050f1b', '47e04994d44f155857264d5c8c5f2a312cc76d3b1f32d0aeb278a96702071cad526b145952cc5025', false, '2023-03-20T10:09:29.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('b756e4ca0fb42048d51caedd411fb20f172d3a1b866207c02650fb9190a36454f9154a0f5f6a66a4', '6e24e9878f80df096880650c23048df401c38d69e75fce2c90003662e74d947a9fc397f86ac0384a', false, '2023-03-20T10:16:18.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('5ef543419647b5c2968fe61d20002794d0bdc30749c3fa25a8ac5053acb4a304971599f43d4dd877', '4d5babad48ab5b0ae48f2062fc09864d9ec8d56974e16cf5effd38e9c6fd85439198457402995e45', false, '2023-03-20T11:34:28.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('6f055ee280586b8b15f3887366aa331aa5b93288ffb01a3994dca3fdefef77dd36ba4b6f6fa45189', 'a739ef2eb2bcac87af2ade2b4886ef29a611740ab21c208809e650d1ab6aa5614ccf28372457ea13', false, '2023-03-20T11:34:59.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('36927a93e8956221923736f71ab802be260bce83c4007689149b4e49dc6484da8f1e6b02ab55d443', '5763173bc9a8ada00b7c93b07a476b5546eeb4b0019525cd9ca7d0f2982769fc0c20f487c61cc908', false, '2023-03-20T11:35:52.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('3688982967c015f1707a51bd13e6b1fafb7127a90e36bf52c869236709b88b4b87d5ab520f87a350', 'c8bb65bed7844efdd5ae702d03828dc17890978c55ab7da8d7276ab3382bdeec0b7f424ca109488a', false, '2023-03-20T11:36:06.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('2626716417bcaf68efde76c283bfcecdd14109e27ac8bcc10257556b0b9950dc36a02030405acf4f', 'e20f3519af63b93370318c9b71fc3ac5cd1d21e934bd19ba87111a5eb731db30e479c3e25713b68e', false, '2023-03-20T11:36:55.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('8f568b9db87cf62517bbc29b8a4eb4a056a124a00ccb6857a7f2b91ad9733d4246efb8965d077633', 'f2efe2f10aee08c6d07a9612e7ba599ef73c28780b868801873b27072576493962b38bc2d90a579c', false, '2023-03-20T11:38:06.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('d5dd9fccfa57daa6cc3e711ba01a472c826a0baf9100ca7cfb737a4ee83eed22526da2a43de163dc', '864d1d7c8046163392a0b710b8f39d238d48d106d44b883f066935a0c63f8af1704fd93172e9ec20', false, '2023-03-20T11:39:18.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('0a085ed657569bd8be7c7daf53d8a2438a7f44cec125e943f6d7f582a647ced0f458110fa656fd8e', '79d6ac70976cad7f664aebf0b713478cbbf5248f044e3c1ca43ca8f7afe353b7a2ebe97cda45ea06', false, '2023-03-20T11:41:52.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('5f19926c811b4091f4433437a1d2bd708b762ab732244eb2d90b51a6751ac782fff82531bc1fdac9', '79e7a84360635ab841f74255072bb8cd3ff806d3b3cb39e94ece984789b4074e713026845ae62892', false, '2023-03-20T11:42:07.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('aeb9177c604235cf5b16818525b48ff1c9b7610c552ac8ee1a4055f4ea769be226a608cea8e108a5', 'c7b4808e29bcc05412fc6675cf65870abd959e9e2825da2f9aab84868814612196de97f95a50c89c', false, '2023-03-20T11:42:19.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('7ad634f15d34c38d30b811176c6908b1b969417cfa938053eb82e4ff0dee12841ce1377eb87a99d0', '30e1f380a8ad2f5b71def0f3410b41b8d0739a9f91cb30f0fbee9f1328336db8b516715080de9c24', false, '2023-03-20T11:42:29.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('a1ca85053f46fe6e0965bf198d3f51d9f7c2ecf085e14d130bab75744ee038e37891ee7c85d8c03d', 'c63f7507767f6e9e68abe477a0cc3bdabfbf287984b269fdd6dc4e2b2939ff73ae430a583a075cc1', false, '2023-03-20T11:42:34.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('0948eb817e94d13c4fdc2758c5e5cecaa3aa62719e2708022535872e19816fdde9a73a2c230e0667', '3ed814242bd079d83aee33a6b93607d258ca91ed5312576e99459153343193b7636a600ccb554df7', false, '2023-03-20T11:42:42.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('3040ee92857fc3140a85ea516543a844fd8d910835ac72e4f9e9075c8ffafd83bf01ddf2bc0a7a90', 'c4d51f05c3b8c4b2023174a7af942d8a2115e88b65f64f6ee8f7991c671ef7a8e6f713164bb48d6f', false, '2023-03-20T11:48:54.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('db3dbbc9dd90f911342ac260771f1b534442788c8fe7fe2b45a1dc12f31e43409ccc0eb0245ac2f7', 'eca165793029b94a354e9ea6eb4d67f420aadb93cdaac42dd4151f336f75bb4edf0186f24690c592', false, '2023-03-20T11:49:41.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('0bcb20c0e28cda1060073da8471daa5a247c426af9618ac9160620b08bdd94ba8e09833256176dae', '82905a6d0b2aca6ba358bbe43d368f3d938d12d4cd9454f344199fa991086dce6ff1482433e2e8fa', false, '2023-03-20T11:50:12.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('f881c9b478c87c41ea3a019a02ed103f6f6fe8caff390872ddca290642713ddaa3f51a590ce2ffbe', 'c7fe9bbe19ffd46fb7f748fab5253ffff4ddbab866246aae06ffb31dc3c666ac79048b2f40036367', false, '2023-03-20T11:50:30.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('dbaabe896fdb95bd2adf91a789404864c798dc05d6a0eaeb5fafe32b8e568f684f4f1e067a090ffc', '850ca47d50d5d0b22a1032ed93fdd1b3fc1f457dd3a01ca1b96f5a21309112254161d03a5ce84f1d', false, '2023-03-20T11:50:36.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('34d8b693c3a0ba729b65cee3daa8b475e7ab71cd134bb6adc9278d47f67369bdf98735e62bccf7d7', 'f26f128713b29991db2c2002da6a0d35ada4ea280eb8e93b5f32c782db5adc5a86cc34df89727c86', false, '2023-03-20T11:51:46.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('7d14f8f0eeafd8665c11d67e61987a6ffcef34c9d075b18e80902091bef83ee52e60e1abe038beb6', '9a6dccb9065df89f6eb5d506d25d6146a54f87b2cd2a96a2b864cdf4b80586d3b0aa55eb41ba5553', false, '2023-03-20T11:51:50.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('619f7f9be04d59aa23b99f6a9bcff4c264753bed452445cc4b3fe62cab2edb96ad3d868b50c9ded8', '14b21a62a612bf874ef46822ea004952ab7dee781ef20c06d7248523f5e635e66d0f4ba0559890d6', false, '2023-03-20T11:53:15.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('ceccc9d1fbacfb73e0c17ed60685e066ac1e0eb28b2f54958de1355578cf3acaa962091f45e40ba8', '2eb48573f3069b36d4f89314ea51c0263bc68720adb3bbaf0d2b395892fc2bd37f075479ab445bbe', false, '2023-03-20T11:53:26.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('88e382dbf2ca180bc83c3e3c743a3f8ee85ab6d1b6921f348db53d075ff976ee0746c8e474828eac', '20f5ddc8f5c061503f33da8220912e806872939ac39e1ea77ef88905a81517fee04d24b8bbbecb4b', false, '2023-03-20T11:53:33.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('21b38a067cf6b554fc9262167a567dadf5e4c4d949118fb50d522a84846345f4f034cc4d69a3dbc8', '15f506ea9f00c953675b3c0d9df3fff485e35ed5509e7f908b23ba27f87db50da0691d0ba8921cee', false, '2023-03-20T11:54:16.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('25cc763fae8dd3551d08e491aa060afb159a311384cded8b4cd2a41271baad8504c208b2a1309e24', '09808f7f31468c6ecdec3bf11c56bd1cb77dfff7eb1d0ceba6e34db5af1d0c33b9311f65a30c9722', false, '2023-03-20T11:54:53.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('9f4b9379adc455733713068539cfd67964388255fa5a4c55705f9bbfbced9ce77a6a4a0f2a01aaa1', '01d0d5fe8dbdea115d0fc87a83bcc0d98d0e03518370f21d49bc17e1eb0a8ac2a5c07bfb4eba22d3', false, '2023-03-20T11:55:09.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('46d2a228916b0255e01ea798079f61e63e93b3e0bba088f280ff7a27e318adedc8ea325f35cb4f06', 'dea14de8aa9178af4abf8b5ba6b9fe45cb351ea35211e38fdcdfddf89b9bae1a84193640555f5426', false, '2023-03-20T11:56:47.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('b5afe5ea1d39ba9b07243dfbb551aa2df125fdfb89656b73248d321178ccbfd96ab9fbf0c2bac931', '5459a5fcb018b05767bb5b920d6d68db39e23741fe5d5479939c316e0f373dbf2898a948e5614e8f', false, '2023-03-20T11:57:31.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('cdd5d6b516ccbd61824ec8b9365b7c5f56a7e1befc93b4bb36f37ee7499b1dac53c931fe765fafbb', '47b042975d90f85f1ddeadf75581b550bc35473f35cc5038f4f0a383e64d5ebaa999234cc1fadd79', false, '2023-03-20T11:59:00.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('be6665c376f77892e56f43294c89c0f20cd86f3f92032a8846ffa77c9c90062d43d017bf39627d3d', '3d03320a98c65965583b5abaabe8df5b17e12aed923cbc4d8b4a2b52f8c573fe9cfa88f746c50bd0', false, '2023-03-20T11:59:27.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('55bbeb447c8041df333ab10bd2ef7f75838e9594e50ca5e3f323a638c0840053d6acdffe8bc56df9', '13f6759821a1ff28fe39c5490360a934e19c3af34e2b2e12390ab34132fb1f36bbc3cf04f8bb50b3', false, '2023-03-20T11:59:51.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('38981bc27f0420a0d3d1f8dff5ce3beefafbe92833675a5abe68f4b643639b13c6a7b36aa52226e8', 'd06c96859388c8d8cef353446924c536df2467582511ecce63feb22fbddfec90cd560ddebdc6d784', false, '2023-03-20T12:01:26.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('9e8b6bfb1ba6ef0e73c78d6ac9d6299b76dbb05517b954a7c4d25c7e56be70b113f57d3ee8298ee7', '33dd0a206c538276c3ae74a6d1df796cb1700ad57420d4ac4e42554f4216b0e75a8f9571dbd91444', false, '2023-03-20T12:01:39.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('aaacb0b1d8c8147be3ffc53e070946c06b5ba9ee40e8597469708db685fa8db242b3b4fe9fff197f', '7b5f8614b6a990fea6a75cc75e4bd1e6dcc3fecfc9ff6ec7c7b44c6608ebb244639e80a695f0d812', false, '2023-03-20T12:02:39.000Z');
INSERT INTO "public"."oauth_refresh_tokens" ("id", "access_token_id", "revoked", "expires_at") VALUES ('d7c2d1ba97ec40019e0bfa46dd473a58f80d86387be5d1d107e839a118d006dc9a8f8d78fac31e7e', '1be253162ea7403ee60d130231619695e4f2fd80ae63e77f0e4d9161743a31dc8896ce3aa699aa6f', false, '2023-03-20T12:03:13.000Z');
INSERT INTO "public"."supplier" ("id", "public_id", "name", "country", "state", "city", "street", "zip", "cp", "pcp", "acp", "created_at", "updated_at") VALUES ('1', '69bd1045-1703-4058-a903-37c8288413c9', 'AssetData.IO', 'ID', 'DKI JAKARTA', 'JAKARTA', 'Jln.Gatot Subroto No. 1', '1234', 'Hasbi Nurhaqi (081316628806)', 'Lokapita (081316628804)', 'Tuki (081316628805)', '2022-03-19T20:56:12.000Z', '2022-03-19T20:56:12.000Z');
INSERT INTO "public"."supplier" ("id", "public_id", "name", "country", "state", "city", "street", "zip", "cp", "pcp", "acp", "created_at", "updated_at") VALUES ('4', '4096a78b-6885-4334-95ed-684cc42cedac', 'SUPPLIER BATU BARA', 'ID', 'WEST JAVA', 'CIREBON', 'TUPARVE NO.2', '1234', 'DADON', 'DADON', 'ABY', '2022-03-20T10:52:16.000Z', '2022-03-20T10:52:16.000Z');
INSERT INTO "public"."supplier" ("id", "public_id", "name", "country", "state", "city", "street", "zip", "cp", "pcp", "acp", "created_at", "updated_at") VALUES ('5', '14e0f1ad-058c-48cc-bf02-1e6acc3d845b', 'SUPPLIER BATU BARA', 'ID', 'WEST JAVA', 'CIREBON', 'TUPARVE NO.2', '1234', 'DADON', 'DADON', 'ABY', '2022-03-20T10:53:43.000Z', '2022-03-20T10:53:43.000Z');
INSERT INTO "public"."supplier" ("id", "public_id", "name", "country", "state", "city", "street", "zip", "cp", "pcp", "acp", "created_at", "updated_at") VALUES ('2', '127d314c-7c21-401c-b694-c52ed5ef102c', 'SUPPLIER BATU BATAS', 'ID', 'WEST JAVA', 'CIREBON', 'TUPARVE NO.2', '1234', 'DADON', 'DADON', 'ABY', '2022-03-20T10:51:12.000Z', '2022-03-20T10:58:57.000Z');
INSERT INTO "public"."users" ("id", "public_id", "firstname", "lastname", "email", "phonenumber", "password", "address", "photo", "login_at", "login_loc", "user_agent", "created_at", "updated_at", "avatar") VALUES ('5', '96aedfbe-1987-4e69-913b-964a03ed4157', 'abiey', 'dadon', 'abydadon@gmail.com', '081316628807', '$2y$10$wGdZ..PHh/eEMZ5ej6x35u7Xg5xLF6O//gbt3MHZQtFczOO8e0ghy', 'Bogor', NULL, NULL, NULL, NULL, '2022-03-20T10:09:28.000Z', '2022-03-20T10:09:28.000Z', NULL);
INSERT INTO "public"."users" ("id", "public_id", "firstname", "lastname", "email", "phonenumber", "password", "address", "photo", "login_at", "login_loc", "user_agent", "created_at", "updated_at", "avatar") VALUES ('1', '723c60ba-56dc-4f4d-95a4-ea3a6f47c186', 'Hasbi', 'Nurhaqi', 'hasbinurhaqy@gmail.com', '081316628806', '$2y$10$v1ZuqYy19IZFbjusglwCzuGPqy.X2g.6qzWT9LN5w3F0MVcKsE3NC', 'Perumahan Griya Soka Blok U No.30, Kab.Bogor', 'uploads/8GBA0Iksm39TrDgMSId6YXCncN0piQ1vORgmexF0.png', '2022-03-20T00:03:14.000Z', '{"latitude":"1.3048","longitude":"103.8622"}', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.74 Safari/537.36', '2022-03-19T20:56:12.000Z', '2022-03-20T12:56:55.000Z', NULL);
ALTER TABLE "public"."migrations" ENABLE TRIGGER ALL;
ALTER TABLE "public"."oauth_access_tokens" ENABLE TRIGGER ALL;
ALTER TABLE "public"."oauth_auth_codes" ENABLE TRIGGER ALL;
ALTER TABLE "public"."oauth_clients" ENABLE TRIGGER ALL;
ALTER TABLE "public"."oauth_personal_access_clients" ENABLE TRIGGER ALL;
ALTER TABLE "public"."oauth_refresh_tokens" ENABLE TRIGGER ALL;
ALTER TABLE "public"."personal_access_tokens" ENABLE TRIGGER ALL;
ALTER TABLE "public"."social_providers" ENABLE TRIGGER ALL;
ALTER TABLE "public"."supplier" ENABLE TRIGGER ALL;
ALTER TABLE "public"."users" ENABLE TRIGGER ALL;
ALTER TABLE "public"."users_meta" ENABLE TRIGGER ALL;
